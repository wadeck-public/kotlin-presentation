package demo.feat2_classSyntax

class Empty

class Student0 {
    var firstName: String? = "John"
    var lastName: String? = "Doe"
    var age: Int? = 10
}

class Student1 constructor(firstName: String?, lastName: String?, age: Int?){
}

class Student2(firstName: String?, lastName: String?, age: Int?){
}

class Student3(firstName: String?, lastName: String?, age: Int?)

class Student4(val firstName: String, val lastName: String, val age: Int = 10){
    val fullName = firstName + ' ' + lastName.toUpperCase()

    init {
        assert(age > 0)
    }
}

data class Student5(val firstName: String? = "John", val lastName: String? = "Doe", val age: Int? = 0)

  fun t(){}

fun testStudent(){
    val s = Student0()

    val student = Student4("Jane", "Doe")

    val student2 = Student5("Jane", "Doe")
    // definition: fun copy(firstName: String = this.firstName, lastName: String = this.lastName, age: Int = this.age) = Student5(firstName, lastName, age)
    student2.copy()
    student2.copy("Toto")
    student2.copy(lastName = "Smith")
}
