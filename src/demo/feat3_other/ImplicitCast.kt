package demo.feat3_other

open class TreeNode
class Leaf(val value: Any) : TreeNode()
class Node(val left: TreeNode, val right: TreeNode) : TreeNode()

fun navigate1(treeNode: TreeNode) {
    if (treeNode is Leaf) {
        println(treeNode.value)
    } else if (treeNode is Node) {
        navigate1(treeNode.left)
        navigate1(treeNode.right)
    }
}

fun navigate2(treeNode: TreeNode) {
    if (treeNode is Leaf) {
        println(treeNode.value)
    } else if (treeNode is Node) {
        navigate2(treeNode.left)
        navigate2(treeNode.right)
    } else {
        throw RuntimeException("Unknown type of node")
    }
}