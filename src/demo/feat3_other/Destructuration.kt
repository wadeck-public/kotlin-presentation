package demo.feat3_other

data class Person(val firstName: String = "John", val lastName: String = "Doe")

fun testPerson() {
    val john = Person()
    val (first, last) = john

    val p1 = Person()
    val p2 = Person()

    for ((first, last) in arrayOf(john, p1, p2)) {
        println("firstName = $first")
    }

    for ((first, _) in arrayOf(john, p1, p2)) {
        println("firstName = $first")
    }

    for ((k, v) in HashMap<String, Int>()) {
        println("key=$k, value=$v")
    }
}

fun main(args: Array<String>) {
    val a = intArrayOf(1, 2, 3, 4)

    val (a1, _, _, a4) = a

    println("$a1 - $a4")
}