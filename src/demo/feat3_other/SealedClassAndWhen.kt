package demo.feat3_other

sealed class TreeNode2
class Leaf2(val value: Any) : TreeNode2()
class Node2(val left: TreeNode2, val right: TreeNode2) : TreeNode2()

fun navigate3(treeNode: TreeNode2):Unit = when(treeNode){
    is Leaf2 -> println(treeNode.value)
    is Node2 -> {
        navigate3(treeNode.left)
        navigate3(treeNode.right)
    }
}