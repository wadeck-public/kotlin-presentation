package demo.feat1_basicSyntax

fun staticFunction() {
    // implicit type
    val constant = "toto";

    var variable = 0;
}

fun sum1(a: Int, b: Int): Int {
    return a + b;
}

fun sum2(a: Int, b: Int) = a + b
fun product(a: Int, b: Int) = a * b

class Product3(val price: Int) {
}

fun sumPriceOfProducts(p1: Product3?, p2: Product3?): Int {
    return (p1?.price ?: 0) + (p2?.price ?: 0)
}

fun sumPriceOfProducts2(p1: Product3?, p2: Product3?) = (p1?.price ?: 0) + (p2?.price ?: 0)

fun sumPriceOfProducts3(vararg ps: Product3?) = ps.sumBy { it?.price ?: 0 }

fun cubeVolume(height:Int = 1, width:Int = 1, depth: Int = 1) = height * width * depth


fun main(args: Array<String>) {

    cubeVolume(3, 1, 1)
    cubeVolume(3)
    cubeVolume(height = 3)
    cubeVolume(depth = 5)

    assert(3 == sumPriceOfProducts(Product3(1), Product3(2)))
    assert(1 == sumPriceOfProducts(Product3(1), null))
    assert(0 == sumPriceOfProducts(null, null))

    assert(0 == sumPriceOfProducts2(null, null))

    assert(4 == sumPriceOfProducts3(null, Product3(4), null))
}