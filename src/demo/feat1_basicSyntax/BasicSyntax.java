package demo.feat1_basicSyntax;

import java.util.Objects;
import java.util.stream.Stream;

class Adder {
    public static int sum(int a, int b) {
        return a + b;
    }
}

class Product {
    Integer price;
}

class Product2 {
    private Integer price;

    public Integer getPrice() {
        return price;
    }
}

class PriceAdder {
    public static int forProduct1(Product p1, Product p2) {
        if (p1 == null) {
            if (p2 == null) {
                return 0;
            } else {
                return p2.price;
            }
        } else {
            if (p2 == null) {
                return p1.price;
            } else {
                return p1.price + p2.price;
            }
        }
    }

    public static int forProduct2(Product p1, Product p2) {
        int p1Price = p1 != null ? p1.price : 0;
        int p2Price = p2 != null ? p2.price : 0;

        return p1Price + p2Price;
    }

    public static int forProduct3(Product2... ps) {
        return Stream.of(ps)
            .filter(Objects::nonNull)
            .mapToInt(Product2::getPrice)
            .sum();
    }
}