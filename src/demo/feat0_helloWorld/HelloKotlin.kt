package demo.feat0_helloWorld

// step 1
fun main(args: Array<String>){
    println("Hello World!")
}

//// step 2
//fun main(args: Array<String>){
//    println("Hello ${args[0]}!")
//}
//
//// step 3
//fun main(args: Array<String>) {
//    args.forEach {
//        println("Hello $it!")
//    }
//}
//
//// step 4
//fun main(args: Array<String>) {
//    arrayOf("Paul", "Nicolas", "Jean").forEach {
//        println("Hello $it!")
//    }
//}

//// step 5
//fun main(vararg args: String){
//    println("Hello World!")
//}
